# JWT-echo

Ini adalah contoh asas untuk integrate JWT ke dalam framework Echo dan boleh menukar public & private keys ketika runtime untuk revoke token terdahulu.

## Cara Menjalankan

```go
go mod download
go mod tidy
go run main.go
```

- buka di browser `http://localhost:3004` dan masukkkan username `user` dan password `user123` untuk dapatkan token JWT.
- jalankan POST request ke `http://localhost:3004/restricted` menggunakan REST client seperti `Postman` dan masuk header `Authorization Bearer` dan token yang tadi.
- jalankan `http://localhost:3004/change_keys` di browser untuk menukar private dan public keys ketika runtime.
- lakukan ujian seperti nombor 2 menggunakan token yang sama. Didapati token tidak sah kerana telah berlaku pertukaran public dan private keys.
