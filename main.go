package main

import (
	"echo-jwt-example/keys"
	"echo-jwt-example/middleware"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/pascaldekloe/jwt"
)

func init() {
	if _, err := os.Stat("./.env"); err != nil {
		if err := keys.GenKeys(); err != nil {
			println(err)
		}
	}
}

func main() {

	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		file, _ := os.ReadFile("home.html")
		return c.HTML(200, string(file))
	})

	e.POST("/login", func(c echo.Context) error {
		user := struct {
			Username string `json:"username" form:"username"`
			Password string `json:"password" form:"password"`
		}{}

		if err := c.Bind(&user); err != nil {
			println(err)
			return c.HTML(400, "<p>Bad Request</p>")
		}

		if !(user.Username == "user" && user.Password == "user123") {
			return c.HTML(401, "<p>wrong combination of username and password</p>")
		}

		var claims jwt.Claims

		t := jwt.NewNumericTime(time.Now().Round(time.Second))

		claims.Issued = t
		claims.Set = map[string]interface{}{"user": user.Username}
		claims.NotBefore = t

		token, err := keys.JWTtoken(claims)
		if err != nil {
			return c.HTML(500, "<p>"+err.Error()+"</p>")
		}

		return c.HTML(200, "<div><p>JWT Token:</p></div><div>"+string(token)+"</div>")

	})

	// use with REST client like Postman.
	// add header "Athorization: Bearer <token>"
	e.GET("/restricted", func(c echo.Context) error {
		loc, err := time.LoadLocation("Asia/Kuala_Lumpur")
		if err != nil {
			loc, _ = time.LoadLocation("UTC")
		}

		claims := c.Get("user").(*jwt.Claims)
		issue_at := claims.Issued.Time().In(loc).Format(time.RFC822)
		user, _ := claims.String("user")
		return c.HTML(200, "<p>grant access to restricted page.</p><p>Issue at: "+issue_at+"</p><p>User:"+user+"</p>")
	}, middleware.JWTverify())

	e.GET("/change_keys", func(c echo.Context) error {
		if err := keys.GenKeys(); err != nil {
			return c.HTML(500, "<p>cannot generate key pairs: "+err.Error()+"</p>")
		}
		return c.HTML(200, "<p>key pairs changed.</p>")
	})

	if err := e.Start("0.0.0.0:3004"); err != nil {
		println(err)
	}
}
