package keys

import (
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"encoding/hex"
	"net/http"
	"time"

	"echo-jwt-example/env"

	"github.com/pascaldekloe/jwt"
	"github.com/pkg/errors"
)

func GenKeys() error {
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		return err
	}

	b, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		return err
	}

	private_key := hex.EncodeToString(b)

	c, err := x509.MarshalPKIXPublicKey(pub)
	if err != nil {
		return err
	}

	public_key := hex.EncodeToString(c)

	if err := env.SetPrivPubKeys(private_key, public_key); err != nil {
		return err
	}

	return nil
}

func PrivateKey() (ed25519.PrivateKey, error) {

	decode, err := hex.DecodeString(env.GetPrivateKey())
	if err != nil {
		return nil, err
	}

	priv, err := x509.ParsePKCS8PrivateKey(decode)
	if err != nil {
		return nil, err
	}

	return priv.(ed25519.PrivateKey), nil
}

func PublicKey() (ed25519.PublicKey, error) {

	decode, err := hex.DecodeString(env.GetPublicKey())
	if err != nil {
		return nil, err
	}

	pub, err := x509.ParsePKIXPublicKey(decode)
	if err != nil {
		return nil, err
	}

	return pub.(ed25519.PublicKey), nil
}

func JWTtoken(claims jwt.Claims) ([]byte, error) {
	p, err := PrivateKey()
	if err != nil {
		return nil, err
	}

	token, err := claims.EdDSASign(p)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func JWTverify(r *http.Request) (*jwt.Claims, error) {

	decode, err := hex.DecodeString(env.GetPublicKey())
	if err != nil {
		return nil, err
	}

	pub, err := x509.ParsePKIXPublicKey(decode)
	if err != nil {
		return nil, err
	}

	claims, err := jwt.EdDSACheckHeader(r, pub.(ed25519.PublicKey))
	if err != nil {
		return nil, err
	}

	if !claims.Valid(time.Now()) {
		return nil, errors.New("invalid token")
	}

	return claims, nil
}
