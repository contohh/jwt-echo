package middleware

import (
	"echo-jwt-example/keys"
	"log"

	"github.com/labstack/echo/v4"
)

func JWTverify() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			log.Println(c.Path())

			claims, err := keys.JWTverify(c.Request())
			if err != nil {
				log.Println(err)
				return c.HTML(401, "<p>Unathorized</p>")
			}

			c.Set("user", claims)
			return next(c)
		}
	}
}
