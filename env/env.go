package env

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var Env map[string]string
var err error

func init() {
	if _, err := os.Stat("./.env"); err == nil {
		Env, err = godotenv.Read("./.env")
		if err != nil {
			log.Println("Env error:", err)
		}
	}
}

func GetPrivateKey() string {
	return Env["PRIVATE_KEY"]
}

func GetPublicKey() string {
	return Env["PUBLIC_KEY"]
}

func SetPrivPubKeys(privateKey, publicKey string) error {
	if Env == nil {
		Env = make(map[string]string)
	}

	Env["PRIVATE_KEY"] = privateKey
	Env["PUBLIC_KEY"] = publicKey

	if err := godotenv.Write(Env, "./.env"); err != nil {
		return err
	}

	Env, err = godotenv.Read("./.env")
	if err != nil {
		return err
	}

	return nil
}
